document.addEventListener('DOMContentLoaded', function () {
    const btnCargar = document.getElementById('btnCargar');
    const btnCargarImg = document.getElementById('btnCargarImg');

    // Evento para cargar las razas de perros al presionar el botón
    btnCargar.addEventListener('click', fetchDogBreeds);

    // Evento para cargar la imagen al presionar el botón
    btnCargarImg.addEventListener('click', fetchDogImage);
});

function fetchDogBreeds() {
    const breedsSelect = document.getElementById('breeds');

    // Limpia el select antes de cargar las razas
    breedsSelect.innerHTML = '<option value="">Selecciona una raza</option>';

    // Llena el select con las razas obtenidas de la API usando Axios
    axios.get('https://dog.ceo/api/breeds/list')
        .then(response => {
            const breeds = response.data.message;
            breeds.forEach(breed => {
                console.log(breed)
                const option = document.createElement('option');
                option.value = breed;
                option.textContent = breed;
                breedsSelect.appendChild(option);
            });
        })
        .catch(error => console.error('Error al obtener la lista de razas:', error));
}

function fetchDogImage() {
    const selectedBreed = document.getElementById('breeds').value;
    const dogImageContainer = document.getElementById('dogImageContainer');

    // Verifica si se ha seleccionado una raza antes de cargar la imagen
    if (!selectedBreed) {
        alert('Selecciona una raza antes de cargar la imagen.');
        return;
    }

    // Llama a la API para obtener una imagen aleatoria de la raza seleccionada usando Axios
    axios.get(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
        .then(response => {
            const imageUrl = response.data.message;

            // Muestra la imagen en el contenedor
            dogImageContainer.innerHTML = `<img src="${imageUrl}" alt="${selectedBreed}">`;
        })
        .catch(error => console.error('Error al obtener la imagen:', error));
}
