const fetchCall = async(pais) => {
    const url = 'https://restcountries.com/v3.1/name/'+pais;
    try{
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        document.getElementById('capital').value = resultado[0].capital;
        document.getElementById('pais').value = resultado[0].name['common'];
        arrLenguajes = resultado[0].languages;
        lenguajesOutput = "";
        for (var language in arrLenguajes) {
            if (arrLenguajes.hasOwnProperty(language)) {
                lenguajesOutput += arrLenguajes[language] +", ";
            }
        }
        document.getElementById('lenguaje').value = lenguajesOutput;
    }catch(error){
        console.error(error);
        alert('No se encuentra este País');
        limpiar();
    }
}

document.getElementById('btnCargar').addEventListener("click", function(){
    pais = document.getElementById('pais').value;
    if(!pais){
        alert("Ingrese el nombre de un país");
        limpiar();
    } else{
        fetchCall(pais);
    }
});

const limpiar = () => {
    document.getElementById('capital').value = "";
    document.getElementById('lenguaje').value = "";
    document.getElementById('pais').value = "";
}
