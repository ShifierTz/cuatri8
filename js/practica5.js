const llamandoFetch =()=>{
    const url = "https://jsonplaceholder.typicode.com/todos"
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(data => mostrarTodos(data))
    .catch((reject)=>{
        console.log("surgio un error" +reject)
    })
}
const llamandoAwait = async () =>{
    try{
        const url = "https://jsonplaceholder.typicode.com/todos"
        const respuesta = await fetch(url)
        const data = await respuesta.json()
        mostrarTodos(data);
    }
    catch(error){
        console.log("surgio un error" + error);
    }
}

const llamandoAxion = async() =>{
    const url = "https://jsonplaceholder.typicode.com/todos"
    axios
    .get(url)
    .then ((res)=>{
        mostrarTodos(res.data)
    })
    .catch((err)=>{
        console.log("surgio un error" + err);
    })
}
const mostrarTodos=(data)=>{
    console.log(data)
    const res = document.getElementById("respuesta");
    res.innerHTML ="";
    for(let item of data){
        res.innerHTML+=item.userId + "," + item.id + "," + item.tittle + "," + item.complet
    }
}

document.getElementById("btnCargarP").addEventListener("click",llamandoFetch);
document.getElementById("btnCargarA").addEventListener("click",llamandoAwait);
document.getElementById("btnCargarAx").addEventListener("click",llamandoAxion);
document.getElementById("btnCargarAx").addEventListener("click",llamandoAxion);
document.getElementById("btnLimpiar").addEventListener('click', function(){
    let res = document.getElementById('respuesta');
    res.innerHTML=""; 

});
